package ${targetPackage};

import com.lizhivscaomei.jes.common.service.EntityService;
import ${ATTR_BASE_RECORD_TYPE};

public interface ${entityName}Service extends EntityService<${entityName}>{}
