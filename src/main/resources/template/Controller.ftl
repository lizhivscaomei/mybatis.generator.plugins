package ${targetPackage};



import com.github.pagehelper.PageInfo;
import com.lizhivscaomei.jes.common.entity.Msg;
import com.lizhivscaomei.jes.common.entity.Page;
import com.lizhivscaomei.jes.common.exception.AppException;
import ${ATTR_BASE_RECORD_TYPE};
import com.lizhivscaomei.jes.sys.service.${entityName}Service;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/com/lizhivscaomei/jes/sys/controller")
public class ${entityName}Controller {
    @Autowired
    ${entityName}Service ${entityNameFirstSmall}Service;

    /**
    * 保存
    * */
    @ResponseBody
    @RequestMapping("/${entityNameFirstSmall}/save")
    public Msg add(${entityName} entity){
        Msg msg=new Msg();
        try {
            if(StringUtils.isNotEmpty(entity.getId())){
                this.${entityNameFirstSmall}Service.update(entity);
            }else {
                this.${entityNameFirstSmall}Service.add(entity);
            }
            msg.setSuccess(true);
        } catch (AppException e) {
            msg.setSuccess(false);
            msg.setInfo(e.getMessage());
        }
        return msg;
    }

    /**
    * 删除
    * */
    @ResponseBody
    @RequestMapping("/${entityNameFirstSmall}/delete")
    public Msg update(String id){
        Msg msg=new Msg();
        try {
            this.${entityNameFirstSmall}Service.delete(id);
            msg.setSuccess(true);
        } catch (AppException e) {
            msg.setSuccess(false);
            msg.setInfo(e.getMessage());
        }
        return msg;
    }

    /**
    * 详情
    * */
    @ResponseBody
    @RequestMapping("/${entityNameFirstSmall}/query/detail")
    public Msg detail(String id){
        ${entityName} entity= this.${entityNameFirstSmall}Service.getById(id);
        Msg msg=new Msg();
        msg.setSuccess(true);
        msg.setData(entity);
        return msg;

    }
    /**
    * 分页查询
    * */
    @ResponseBody
    @RequestMapping("/${entityNameFirstSmall}/query/page")
    public Msg update(${entityName} entity, Page page){
        PageInfo<${entityName}> pages = this.${entityNameFirstSmall}Service.queryPage(entity, page);
        Msg msg=new Msg();
        msg.setSuccess(true);
        msg.setData(pages);
        return msg;

    }

}

