package ${targetPackage};

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lizhivscaomei.jes.common.entity.Page;
import com.lizhivscaomei.jes.common.exception.AppException;
import ${ATTR_BASE_RECORD_TYPE};
import ${ATTR_MYBATIS3_JAVA_MAPPER_TYPE};
import ${ATTR_EXAMPLE_TYPE};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;
@Service
public class ${entityName}ServiceImp implements ${entityName}Service {
    @Autowired
    ${entityName}Mapper ${entityNameFirstSmall}Mapper;

    @Override
    @Transactional
    public void add(${entityName} entity) throws AppException {
        if(entity!=null){
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
            //默认数据
            entity.setId(UUID.randomUUID().toString());
            entity.setCreateBy(userDetails.getUsername());
            entity.setCreateDate(new Date());
            entity.setUpdateBy(userDetails.getUsername());
            entity.setUpdateDate(new Date());
            entity.setDelFlag("0");
            this.${entityNameFirstSmall}Mapper.insertSelective(entity);
        }else {
            throw new AppException("entity不可为空");
        }
    }

    @Override
    @Transactional
    public void update(${entityName} entity) throws AppException {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        entity.setUpdateBy(userDetails.getUsername());
        entity.setUpdateDate(new Date());
        this.${entityNameFirstSmall}Mapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    @Transactional
    public void delete(String id) throws AppException {
        this.${entityNameFirstSmall}Mapper.deleteByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ${entityName} getById(String id) {
        return this.${entityNameFirstSmall}Mapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = true)
    public PageInfo<${entityName}> queryPage(${entityName} entity, Page page) {
        ${entityName}Example example=new ${entityName}Example();
        PageHelper.startPage(page.getCurrentPage(),page.getPageSize());
        List<${entityName}> list= this.${entityNameFirstSmall}Mapper.selectByExample(example);
        return new PageInfo<${entityName}>(list);
    }
}
