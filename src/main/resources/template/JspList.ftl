<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${entityName}</title>
    <!-- 引入样式 -->
    <link rel="stylesheet" href="/static/component/element/css/index.css">

</head>
<body style="background-color: white">
<div id="app">

    <hr style="border: none">
    <div>
        <el-button size="mini" type="primary" @click="showEditForm('add')" icon="el-icon-plus">添加</el-button>
        <hr style="border: none">
        <el-form :inline="true" :model="queryForm.data" class="demo-form-inline">
            <el-form-item label="">
                <el-input v-model="queryForm.data.name" placeholder="名称"></el-input>
            </el-form-item>
            <el-form-item>
                <el-button type="primary" @click="queryFormSubmit" icon="el-icon-search">查询</el-button>
            </el-form-item>
        </el-form>
    </div>
    <hr>
    <div>
        <el-table :data="tableData" v-loading="loading" style="width: 100%">
            <el-table-column label="序号" type="index"></el-table-column>
        <#list introspectedTable.baseColumns as column>
            <el-table-column label="${column.remarks}" prop="${column.javaProperty}"></el-table-column>
        </#list>
            <el-table-column label="操作" fixed="right" width="200" prop="id">
                <template slot-scope="scope">
                    <el-button size="mini" type="primary" @click="showEditForm('edit',scope.$index, scope.row)" icon="el-icon-edit">编辑</el-button>
                    <el-button size="mini" type="danger" @click="deleteItem(scope.$index, scope.row)" icon="el-icon-delete">删除</el-button>
                </template>
            </el-table-column>
        </el-table>

        <el-pagination @size-change="handleSizeChange" @current-change="handleCurrentChange"
                       :current-page="pages.currentPage" :page-sizes="pages.pageSizeList"
                       :page-size="pages.pageSize" layout="total, sizes, prev, pager, next, jumper" :total="pages.total"
                       prev-text="上一页"
                       next-text="下一页" background>
        </el-pagination>
    </div>
    <div>
        <el-dialog :title="editForm.formName" :visible.sync="editForm.dialogFormVisible">
            <el-form ref="editForm.data" :model="editForm.data" :rules="editForm.rules" label-width="120px"
                     label-position="right">
                <el-row>
                <#list introspectedTable.baseColumns as column>
                    <el-col :span="12">
                        <#if column_index%2==1>
                            <el-form-item label="${column.remarks}" prop="${column.javaProperty}">
                                <el-input v-model="editForm.data.${column.javaProperty}" clearable></el-input>
                            </el-form-item>
                        </#if>
                    </el-col>
                </#list>
                <#list introspectedTable.baseColumns as column>
                    <el-col :span="12">
                        <#if column_index%2==0>
                            <el-form-item label="${column.remarks}" prop="${column.javaProperty}">
                                <el-input v-model="editForm.data.${column.javaProperty}" clearable></el-input>
                            </el-form-item>
                        </#if>
                    </el-col>
                </#list>
                </el-row>

                <input type="hidden" v-model="editForm.data.id">
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="editForm.dialogFormVisible = false">取 消</el-button>
                <el-button type="primary" @click="save('editForm.data')">保 存</el-button>
            </div>
        </el-dialog>
    </div>
</div>
</body>

<script src="/static/js/vue.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/component/element/js/index.js"></script>
<script>

    Vue.http.options.emulateJSON = true;
    var vueApp = new Vue({
        el: "#app",
        data: {
            loading: false,
            url: {
                queryPage: "/com/lizhivscaomei/jes/sys/controller/${entityNameFirstSmall}/query/page",
                save: "/com/lizhivscaomei/jes/sys/controller/${entityNameFirstSmall}/save",
                delete: "/com/lizhivscaomei/jes/sys/controller/${entityNameFirstSmall}/delete",
                detail: "/com/lizhivscaomei/jes/sys/controller/${entityNameFirstSmall}/query/detail",
                domainOptions:"/com/lizhivscaomei/jes/sys/controller/sysDomain/query/spinner"
            },
            pages: {
                total: 0,
                currentPage: 1,
                pageSize: 20,
                pageSizeList: [20, 40, 100]
            },
            tableData: [],
            editForm: {
                formName: "${entityName}",
                dialogFormVisible: false,
                data: {
    <#list introspectedTable.baseColumns as column>
    //${column.remarks}
    ${column.javaProperty}:"",
    </#list>
            id:
    ""
    },
    rules: {
    <#list introspectedTable.baseColumns as column>
    ${column.javaProperty}: [{required: true, message: '请输入${column.remarks}', trigger: 'blur'}],
    </#list>
            id:
        []
    }
    },
    queryForm: {
        dialogFormVisible: false,
                data
    :
        {
            currentPage: 1,
                    pageSize
        :
            10,
        <#list introspectedTable.baseColumns as column>
            //${column.remarks}
        ${column.javaProperty}:"",
        </#list>
                id:
            ""
        }
    }
    ,
    options: {
        domainId: [
            {
                "id": "04dbbae9-5fdf-45c4-a6a9-cecfd25e8735",
                "text": "标准化种植"
            }, {
                "id": "51deaa68-df77-4f31-bf8c-51f0a01dfc1c",
                "text": "区块链"
            }, {
                "id": "5a3a051c-e90a-4059-9ad4-672d64a4ef05",
                "text": "价格体系"
            }, {
                "id": "c76b0455-674e-4b98-bb64-74573fc4e4b5",
                "text": "市政桥涵"
            }, {
                "id": "d03cd0ad-c773-43d0-a88d-6e6defac49e3",
                "text": "物联网平台"
            }, {"id": "d926086b-bdb9-4e9f-af4e-e960523e9427", "text": "系统管理"}
        ]
    }
    },
    methods: {
        handleSizeChange: function (val) {
            this.queryFormSubmit();
        },
        handleCurrentChange: function (val) {
            this.queryFormSubmit();
        },
        queryFormSubmit: function () {
            this.loading = true;
            this.queryForm.data.currentPage = this.pages.currentPage;
            this.queryForm.data.pageSize = this.pages.pageSize;
            this.$http.get(this.url.queryPage, {params: this.queryForm.data}).then(function (res) {
                this.loading = false;
                this.tableData = res.data.data.list;
                this.pages.total = res.data.data.total;
            }, function (res) {
                this.loading = false;
                this.$message({
                    showClose: true,
                    message: "网络错误,code=" + res.status,
                    type: "error"
                });
            });
        } ,
        showEditForm: function (action,index, row) {
            if(action=="add"){
                this.editForm.data.id="";
            }
            if(action=="edit"){
                this.editForm.data = row;
            }
            this.editForm.dialogFormVisible = true;
        },
        deleteItem: function (index, row) {
            this.$confirm('此操作将删除' + row.name + ', 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                this.$http.get(this.url.delete, {params: {id: row.id}}).then(function (res) {
                this.$message({
                    type: 'success',
                    message: '删除成功!'
                });
                vueApp.queryFormSubmit();
            }, function (res) {
                this.$message({
                    showClose: true,
                    message: '网络错误' + res.status,
                    type: 'error'
                });
            });
        }).catch(() => {
                this.$message({
                type: 'info',
                message: '已取消删除'
            });
        });
        },
        save: function (formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.$http.post(this.url.save,this.editForm.data).then(function (res) {
                        this.$message({
                            type: 'success',
                            message: '保存成功!'
                        });
                        this.editForm.dialogFormVisible = false;
                        vueApp.queryFormSubmit();
                    }, function (res) {
                        this.loading = false;
                        console.log(res.status);
                        this.$message({
                            showClose: true,
                            message: '网络错误' + res.status,
                            type: 'error'
                        });
                    });

                } else {
                    console.log('error submit!!');

            return false;
        }
        });
        }
    },
    mounted:function () {
            //初始化操作
        /*debugger;
        this.$http.get(this.url.domainOptions).then(function (res) {
            this.options.domainId = res.data;
            this.editForm.data.domainId=res.data[0].id;
            this.queryForm.data.domainId=res.data[0].id;
        }, function (res) {
            this.$message({
                showClose: true,
                message: "网络错误,code=" + res.status,
                type: "error"
            });
        });*/
        this.queryFormSubmit();
    }
    });

</script>
</html>