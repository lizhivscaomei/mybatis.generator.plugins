package com.lizhivscaomei.jes.codegen.mybatis.plugins;

import com.lizhivscaomei.jes.codegen.utils.FreemarkerUtil;
import com.lizhivscaomei.jes.codegen.utils.StringUtil;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonPlugin extends PluginAdapter {
    String templateName;
    public boolean validate(List<String> list) {
        if(this.getProperties().getProperty("templateName")!=null&&this.getProperties().getProperty("targetPackage")!=null&&this.getProperties().getProperty("targetProject")!=null){
            this.templateName=this.getProperties().get("templateName").toString();
            return true;
        }
        return false;
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(this.getProperties().getProperty("overwrite"))&&this.getProperties().getProperty("overwrite").equals("true")){
            //用户自定义的模板文件目录
            String templatePath=System.getProperty("user.dir")+File.separator+ "src"+File.separator+"main"+File.separator+"resources"+File.separator+"template";
            //目标文件（项目根目录+目标模块+包名+文件名）
            String targetFileName=System.getProperty("user.dir");
            targetFileName=targetFileName+File.separator+ this.getProperties().getProperty("targetProject");
            targetFileName=targetFileName+File.separator+(this.getProperties().getProperty("targetPackage").replace(".",File.separator));
            targetFileName=targetFileName+File.separator+StringUtil.firstCharLow(introspectedTable.getFullyQualifiedTable().getDomainObjectName());
            targetFileName=targetFileName+File.separator+StringUtil.firstCharUp(introspectedTable.getFullyQualifiedTable().getDomainObjectName())+this.getProperties().getProperty("targetSuffix");
            //模板参数
            Map<String, Object> dataMap=new HashMap<String,Object>();
            dataMap.put("targetPackage",this.getProperties().getProperty("targetPackage"));//目标包命名空间
            dataMap.put("entityName",introspectedTable.getFullyQualifiedTable().getDomainObjectName());//
            dataMap.put("entityNameFirstSmall", StringUtil.firstCharLow(introspectedTable.getFullyQualifiedTable().getDomainObjectName()));//
            dataMap.put("ATTR_BASE_RECORD_TYPE",introspectedTable.getBaseRecordType());//
            dataMap.put("ATTR_MYBATIS3_JAVA_MAPPER_TYPE",introspectedTable.getMyBatis3JavaMapperType());//
            dataMap.put("ATTR_EXAMPLE_TYPE",introspectedTable.getExampleType());//
            dataMap.put("PrimaryKeyColumns",introspectedTable.getPrimaryKeyColumns());//
            dataMap.put("introspectedTable",introspectedTable);//
            FreemarkerUtil.genFile(templatePath,templateName,targetFileName,dataMap);
            return super.contextGenerateAdditionalJavaFiles(introspectedTable);
        }else {
            return null;
        }

    }
}
