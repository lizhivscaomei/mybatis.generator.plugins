package com.lizhivscaomei.jes.codegen.utils;

public class StringUtil {
    public static String firstCharUp(String name){
        name = name.substring(0, 1).toUpperCase() + name.substring(1);
        return  name;
    }
    public static String firstCharLow(String name){
        name = name.substring(0, 1).toLowerCase() + name.substring(1);
        return  name;
    }
}
