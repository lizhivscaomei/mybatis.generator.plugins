package com.lizhivscaomei.jes.codegen.utils;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.util.Map;

public class FreemarkerUtil {
    /**
     * 根据模板生成文件
     * @param templatePath 模板文件所在路径
     * @param templateName 模板文件名
     * @param targetFileFullName 目标文件全路径
     * @param dataMap 模板文件需要的参数
     * */
    public static void genFile(String templatePath,String templateName,String targetFileFullName,Map<String,Object> dataMap){
        // step1 创建freeMarker配置实例
        Configuration configuration = new Configuration();
        Writer out = null;
        try {
            // step2 获取模版路径
            File templatePathDir = new File(templatePath);
            File templateFile = new File(templatePathDir,templateName);
            if(templatePathDir.exists()&&templateFile.exists()){
                //如果用户自定义模板文件
                configuration.setDirectoryForTemplateLoading(new File(templatePath));
            }else {
                //加载默认模板
                configuration.setClassForTemplateLoading(FreemarkerUtil.class,"/template/");
            }
            // step3 创建数据模型
            // step4 加载模版文件
            Template template = configuration.getTemplate(templateName);
            // step5 生成数据
            File targetFile = new File(targetFileFullName);
            targetFile.getParentFile().mkdirs();
            targetFile.createNewFile();
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(targetFile)));
            // step6 输出文件
            template.process(dataMap, out);
            System.out.println(targetFileFullName+"文件创建成功 !");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != out) {
                    out.flush();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
